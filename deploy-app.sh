#!/bin/bash

echo "Deploying web application..."
flutter build web
firebase deploy

echo "Deploying Android application..."
flutter build apk
firebase appdistribution:distribute build/app/outputs/flutter-apk/app-release.apk \
    --app 1:652184423842:android:842cc24e2183b82036f6fb