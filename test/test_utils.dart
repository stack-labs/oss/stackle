import 'package:flutter_test/flutter_test.dart';
import 'package:tusom/game/game.model.dart';

// ignore_for_file: avoid-dynamic

Matcher matchesWord(String word) {
  return _MatchesWord(word);
}

class _MatchesWord extends Matcher {
  final String word;

  const _MatchesWord(this.word) : super();

  @override
  bool matches(Object? item, Map matchState) {
    return item is Word && item.letters.map((l) => l.char).join() == word;
  }

  @override
  Description describe(Description description) =>
      description.add("matches word '$word'");

  @override
  Description describeMismatch(
    dynamic item,
    Description mismatchDescription,
    Map matchState,
    bool verbose,
  ) {
    final Description description;
    if (item is Word) {
      description = StringDescription('matches word 2 $word')..add('another');
    } else {
      description = StringDescription('item is not word')..add('another');
    }
    return description;
  }
}
