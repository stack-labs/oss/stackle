import 'package:fake_async/fake_async.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:tusom/game/game.cubit.dart';
import 'package:tusom/game/game.model.dart';
import 'package:tusom/game/game.state.dart';
import 'package:tusom/game/game.utils.dart';
import 'package:tusom/settings/settings.model.dart';

import '../test_utils.dart';

const testWord = 'awesome';

void main() {
  late GameCubit game;

  late FakeAsync fakeAsync;

  setUpAll(() {
    TestWidgetsFlutterBinding.ensureInitialized();
  });

  setUp(() {
    game = GameCubit(testWord, Dictionary.english);
    fakeAsync = FakeAsync();
  });

  test('is initialized correctly', () {
    final state = game.state;

    // Guessing word should be uppercase
    expect(state.word, testWord.toUpperCase());

    // Tries should only contain one correctly placed letter
    expect(state.tries, hasLength(maxTries));
    expect(state.tries.first, matchesWord('A......'));
    expect(
      state.tries.first.letters.map((e) => e.status),
      containsAllInOrder([
        GuessLetterStatus.given,
        ...List.generate(6, (_) => GuessLetterStatus.inProgress),
      ]),
    );
    expect(state.tries[1], matchesWord(''));

    // Keyboard should only contain one correctly placed letter
    expect(state.keyboard, hasLength(1));
    expect(
      state.keyboard,
      {'A': const KeyboardLetter('A', KeyboardLetterStatus.correctlyPlaced)},
    );
  });

  test('letters can be added until word is full', () {
    // Given

    // Then
    expectLater(
      game.stream
          .map((event) => event.tries.first)
          .timeout(const Duration(seconds: 1)),
      emitsInOrder([
        matchesWord('AB.....'),
        matchesWord('ABC....'),
        matchesWord('ABCD...'),
        matchesWord('ABCDE..'),
        matchesWord('ABCDEF.'),
        matchesWord('ABCDEFG'),
        // Last letter should be rejected with an error event
        matchesWord('ABCDEFG'),
        // Previous valid event should be re-emitted
        matchesWord('ABCDEFG'),
      ]),
    );

    // When
    game.addLetter('B');
    game.addLetter('C');
    game.addLetter('D');
    game.addLetter('E');
    game.addLetter('F');
    game.addLetter('G');
    game.addLetter('H');
  });

  test('letters can be removed', () {
    // Given (we'll start with word "ABCD")
    game.addLetter('B');
    game.addLetter('C');
    game.addLetter('D');

    // Then
    expectLater(
      game.stream
          .map((event) => event.tries.first)
          .timeout(const Duration(seconds: 1)),
      emitsInOrder([
        matchesWord('ABC....'),
        matchesWord('ABCD...'),
        matchesWord('ABC....'),
        matchesWord('AB.....'),
        matchesWord('A......'),
        // Last letter cannot be removed : should be rejected with an error event
        matchesWord('A......'),
        // Previous valid event should be re-emitted
        matchesWord('A......'),
      ]),
    );

    // When
    game.removeLastLetter();
    game.addLetter('D');
    game.removeLastLetter();
    game.removeLastLetter();
    game.removeLastLetter();
    game.removeLastLetter();
  });

  test('invalid word is rejected on submit', () {
    fakeAsync.run((self) {
      // Given
      game.addLetter('B');
      game.addLetter('C');
      game.addLetter('D');
      game.addLetter('E');
      game.addLetter('F');
      game.addLetter('G');

      // Then
      expectLater(
        game.stream.timeout(const Duration(seconds: 10)),
        emitsInOrder([
          isA<GameWordNotValid>(),
        ]),
      );

      // When
      game.submitWord();
      self.elapse(const Duration(seconds: 1));
    });
  });

  test('word is animated on submit', () {
    fakeAsync.run((self) {
      // Given (we'll start with word "ABCD")
      game.addLetter('R');
      game.addLetter('T');
      game.addLetter('I');
      game.addLetter('C');
      game.addLetter('L');
      game.addLetter('E');

      // Then
      expectLater(
        game.stream.timeout(const Duration(seconds: 10)),
        emitsInOrder([
          isA<GameAnimating>(),
          isA<GameAnimating>(),
          isA<GameAnimating>(),
          isA<GameAnimating>(),
          isA<GameAnimating>(),
          isA<GameAnimating>(),
          isA<GameGuessing>(),
        ]),
      );

      // When
      game.submitWord();
      self.elapse(const Duration(seconds: 3));
    });
  });

  group('word is compared to original word on submit', () {
    void _submitWord(String word, Word matcher) {
      fakeAsync.run((self) {
        for (var i = 1; i < word.length; i++) {
          game.addLetter(word[i]);
        }
        game.submitWord();

        self.elapse(const Duration(seconds: 3));

        expect(game.state.tries.first, matcher);
      });
    }

    test('with word ADDIBLE', () {
      _submitWord(
        'ADDIBLE',
        const Word(
          [
            GuessLetter('A', GuessLetterStatus.given),
            GuessLetter('D', GuessLetterStatus.notPresent),
            GuessLetter('D', GuessLetterStatus.notPresent),
            GuessLetter('I', GuessLetterStatus.notPresent),
            GuessLetter('B', GuessLetterStatus.notPresent),
            GuessLetter('L', GuessLetterStatus.notPresent),
            GuessLetter('E', GuessLetterStatus.correctlyPlaced),
          ],
          isSubmitted: true,
        ),
      );
    });

    test('with word AEROBEE', () {
      _submitWord(
        'AEROBEE',
        const Word(
          [
            GuessLetter('A', GuessLetterStatus.given),
            GuessLetter('E', GuessLetterStatus.misplaced),
            GuessLetter('R', GuessLetterStatus.notPresent),
            GuessLetter('O', GuessLetterStatus.misplaced),
            GuessLetter('B', GuessLetterStatus.notPresent),
            GuessLetter('E', GuessLetterStatus.notPresent),
            GuessLetter('E', GuessLetterStatus.correctlyPlaced),
          ],
          isSubmitted: true,
        ),
      );
    });

    test('with word ADROWSE', () {
      _submitWord(
        'ADROWSE',
        const Word(
          [
            GuessLetter('A', GuessLetterStatus.given),
            GuessLetter('D', GuessLetterStatus.notPresent),
            GuessLetter('R', GuessLetterStatus.notPresent),
            GuessLetter('O', GuessLetterStatus.misplaced),
            GuessLetter('W', GuessLetterStatus.misplaced),
            GuessLetter('S', GuessLetterStatus.misplaced),
            GuessLetter('E', GuessLetterStatus.correctlyPlaced),
          ],
          isSubmitted: true,
        ),
      );
    });

    test('with word AWESOME', () {
      _submitWord(
        'AWESOME',
        const Word(
          [
            GuessLetter('A', GuessLetterStatus.given),
            GuessLetter('W', GuessLetterStatus.correctlyPlaced),
            GuessLetter('E', GuessLetterStatus.correctlyPlaced),
            GuessLetter('S', GuessLetterStatus.correctlyPlaced),
            GuessLetter('O', GuessLetterStatus.correctlyPlaced),
            GuessLetter('M', GuessLetterStatus.correctlyPlaced),
            GuessLetter('E', GuessLetterStatus.correctlyPlaced),
          ],
          isSubmitted: true,
        ),
      );
    });
  });

  group('keyboard', () {
    final _alphabet = alphabet();

    late List<String> correctlyPlaced;
    late List<String> misplaced;
    late List<String> notPresent;

    setUp(() {
      game = GameCubit('sauveur', Dictionary.french);
    });

    List<String> uncovered() {
      return _alphabet
          .where(
            (l) =>
                !correctlyPlaced.contains(l) &&
                !misplaced.contains(l) &&
                !notPresent.contains(l),
          )
          .toList();
    }

    void expectCorrectKeyboard() {
      for (final letter in correctlyPlaced) {
        expect(
          game.state.keyboard[letter]!.status,
          KeyboardLetterStatus.correctlyPlaced,
          reason:
              'Expected $letter to be correctly placed but was actually ${game.state.keyboard[letter]!.status}',
        );
      }
      for (final letter in misplaced) {
        expect(
          game.state.keyboard[letter]!.status,
          KeyboardLetterStatus.misplaced,
          reason:
              'Expected $letter to be misplaced but was actually ${game.state.keyboard[letter]!.status}',
        );
      }
      for (final letter in notPresent) {
        expect(
          game.state.keyboard[letter]!.status,
          KeyboardLetterStatus.notPresent,
          reason:
              'Expected $letter to be "not present" but was actually ${game.state.keyboard[letter]!.status}',
        );
      }
      final _uncoveredLetters = uncovered();
      for (final letter in _uncoveredLetters) {
        if (game.state.keyboard.containsKey(letter)) {
          expect(
            game.state.keyboard[letter]!.status,
            KeyboardLetterStatus.uncovered,
            reason:
                'Expected $letter to be uncovered placed but was actually ${game.state.keyboard[letter]!.status}',
          );
        }
        // A letter not in the keyboard map is an uncovered letter, that's okay too
      }
    }

    void _submitWord(String word, FakeAsync fakeAsync) {
      for (var i = 1; i < word.length; i++) {
        game.addLetter(word[i]);
      }
      game.submitWord();

      fakeAsync.elapse(const Duration(seconds: 3));
    }

    test('only contains the first letter initially', () {
      // Given
      // When
      // Then
      correctlyPlaced = ['S'];
      misplaced = [];
      notPresent = [];

      expectCorrectKeyboard();
    });

    test('contains appropriate letters on successive tries', () {
      fakeAsync.run((self) {
        // First try : "SABLAGE"
        _submitWord('SABLAGE', self);
        correctlyPlaced = ['S', 'A'];
        misplaced = ['E'];
        notPresent = ['B', 'L', 'G'];

        expectCorrectKeyboard();

        // Second try : "SALETÉS"
        game.removeLastLetter();
        _submitWord('SALETES', self);
        correctlyPlaced = ['S', 'A'];
        misplaced = ['E'];
        notPresent = ['B', 'L', 'G', 'T'];

        expectCorrectKeyboard();
      });
    });
  });
}
