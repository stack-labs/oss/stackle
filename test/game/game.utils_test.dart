import 'package:flutter_test/flutter_test.dart';
import 'package:tusom/game/game.utils.dart';

void main() {
  test('alphabet is returned', () {
    final letters = alphabet();
    expect(letters, hasLength(26));
    expect(letters.first, 'A');
    expect(letters[1], 'B');
    expect(letters[2], 'C');
    expect(letters[24], 'Y');
    expect(letters.last, 'Z');
  });
}
