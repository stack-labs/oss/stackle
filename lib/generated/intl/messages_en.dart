// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(dictionary) => "${Intl.select(dictionary, {
            'french': 'french',
            'english': 'english',
            'tech': 'tech',
          })}";

  static String m1(dictionary) => "${Intl.select(dictionary, {
            'french': '🇫🇷 French',
            'english': '🇬🇧 English',
            'tech': '🧑‍💻 Tech',
          })}";

  static String m2(nbTries) => "in ${nbTries} tries !";

  static String m3(dictionary) => "New ${dictionary} game";

  static String m4(dictionary) => "${dictionary} words";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "dictionary": m0,
        "dictionaryWithFlag": m1,
        "gameInstructions": MessageLookupByLibrary.simpleMessage(
            "6 tries to guess a dictionary word"),
        "gameLostOops": MessageLookupByLibrary.simpleMessage("Oops !"),
        "gameLostTryAgain": MessageLookupByLibrary.simpleMessage(
            "Try again to win an awesome gift 🎁 !"),
        "gameLostWordNotFound":
            MessageLookupByLibrary.simpleMessage("The word to find was"),
        "gameWonCongratz": MessageLookupByLibrary.simpleMessage("Congratz !"),
        "gameWonFoundTheWord":
            MessageLookupByLibrary.simpleMessage("You found the word"),
        "gameWonNbTries": m2,
        "gameWonPickABall": MessageLookupByLibrary.simpleMessage(
            "Pick a ball to reveal your gift 🎁 !"),
        "gameWordNotInDictionary": MessageLookupByLibrary.simpleMessage(
            "This word is not in our dictionary"),
        "home": MessageLookupByLibrary.simpleMessage("Home"),
        "newDictionaryGame": m3,
        "newGame": MessageLookupByLibrary.simpleMessage("New game"),
        "settingsDictionary":
            MessageLookupByLibrary.simpleMessage("Dictionary:"),
        "settingsDictionaryWords": m4,
        "settingsTitle": MessageLookupByLibrary.simpleMessage("Settings"),
        "settingsWordsLength":
            MessageLookupByLibrary.simpleMessage("Words length:")
      };
}
