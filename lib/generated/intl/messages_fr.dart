// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a fr locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'fr';

  static String m0(dictionary) => "${Intl.select(dictionary, {
            'french': 'français',
            'english': 'anglais',
            'tech': 'tech',
          })}";

  static String m1(dictionary) => "${Intl.select(dictionary, {
            'french': '🇫🇷 Français',
            'english': '🇬🇧 Anglais',
            'tech': '🧑‍💻 Tech',
          })}";

  static String m2(nbTries) => "en ${nbTries} essais !";

  static String m3(dictionary) => "Nouvelle partie avec mots ${dictionary}";

  static String m4(dictionary) => "Mots ${dictionary}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "dictionary": m0,
        "dictionaryWithFlag": m1,
        "gameInstructions": MessageLookupByLibrary.simpleMessage(
            "6 tentatives pour trouver un mot du dictionnaire"),
        "gameLostOops": MessageLookupByLibrary.simpleMessage("Perdu !"),
        "gameLostTryAgain": MessageLookupByLibrary.simpleMessage(
            "Retente ta chance pour gagner un cadeau 🎁 !"),
        "gameLostWordNotFound":
            MessageLookupByLibrary.simpleMessage("Il fallait trouver le mot"),
        "gameWonCongratz": MessageLookupByLibrary.simpleMessage("Bravo !"),
        "gameWonFoundTheWord":
            MessageLookupByLibrary.simpleMessage("Tu as trouvé le mot"),
        "gameWonNbTries": m2,
        "gameWonPickABall": MessageLookupByLibrary.simpleMessage(
            "Pioche une balle pour découvrir ton cadeau 🎁 !"),
        "gameWordNotInDictionary": MessageLookupByLibrary.simpleMessage(
            "Ce mot n\'est pas dans notre dictionnaire"),
        "home": MessageLookupByLibrary.simpleMessage("Accueil"),
        "newDictionaryGame": m3,
        "newGame": MessageLookupByLibrary.simpleMessage("Nouvelle partie"),
        "settingsDictionary":
            MessageLookupByLibrary.simpleMessage("Dictionnaire :"),
        "settingsDictionaryWords": m4,
        "settingsTitle": MessageLookupByLibrary.simpleMessage("Paramètres"),
        "settingsWordsLength":
            MessageLookupByLibrary.simpleMessage("Taille des mots :")
      };
}
