// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `6 tries to guess a dictionary word`
  String get gameInstructions {
    return Intl.message(
      '6 tries to guess a dictionary word',
      name: 'gameInstructions',
      desc: '',
      args: [],
    );
  }

  /// `{dictionary, select, french {french} english {english} tech {tech}}`
  String dictionary(String dictionary) {
    return Intl.select(
      dictionary,
      {
        'french': 'french',
        'english': 'english',
        'tech': 'tech',
      },
      name: 'dictionary',
      desc: '',
      args: [dictionary],
    );
  }

  /// `{dictionary, select, french {🇫🇷 French} english {🇬🇧 English} tech {🧑‍💻 Tech}}`
  String dictionaryWithFlag(String dictionary) {
    return Intl.select(
      dictionary,
      {
        'french': '🇫🇷 French',
        'english': '🇬🇧 English',
        'tech': '🧑‍💻 Tech',
      },
      name: 'dictionaryWithFlag',
      desc: '',
      args: [dictionary],
    );
  }

  /// `Home`
  String get home {
    return Intl.message(
      'Home',
      name: 'home',
      desc: '',
      args: [],
    );
  }

  /// `New game`
  String get newGame {
    return Intl.message(
      'New game',
      name: 'newGame',
      desc: '',
      args: [],
    );
  }

  /// `New {dictionary} game`
  String newDictionaryGame(String dictionary) {
    return Intl.message(
      'New $dictionary game',
      name: 'newDictionaryGame',
      desc: '',
      args: [dictionary],
    );
  }

  /// `Settings`
  String get settingsTitle {
    return Intl.message(
      'Settings',
      name: 'settingsTitle',
      desc: '',
      args: [],
    );
  }

  /// `Dictionary:`
  String get settingsDictionary {
    return Intl.message(
      'Dictionary:',
      name: 'settingsDictionary',
      desc: '',
      args: [],
    );
  }

  /// `{dictionary} words`
  String settingsDictionaryWords(String dictionary) {
    return Intl.message(
      '$dictionary words',
      name: 'settingsDictionaryWords',
      desc: '',
      args: [dictionary],
    );
  }

  /// `Words length:`
  String get settingsWordsLength {
    return Intl.message(
      'Words length:',
      name: 'settingsWordsLength',
      desc: '',
      args: [],
    );
  }

  /// `This word is not in our dictionary`
  String get gameWordNotInDictionary {
    return Intl.message(
      'This word is not in our dictionary',
      name: 'gameWordNotInDictionary',
      desc: '',
      args: [],
    );
  }

  /// `Congratz !`
  String get gameWonCongratz {
    return Intl.message(
      'Congratz !',
      name: 'gameWonCongratz',
      desc: '',
      args: [],
    );
  }

  /// `You found the word`
  String get gameWonFoundTheWord {
    return Intl.message(
      'You found the word',
      name: 'gameWonFoundTheWord',
      desc: '',
      args: [],
    );
  }

  /// `in {nbTries} tries !`
  String gameWonNbTries(int nbTries) {
    return Intl.message(
      'in $nbTries tries !',
      name: 'gameWonNbTries',
      desc: '',
      args: [nbTries],
    );
  }

  /// `Pick a ball to reveal your gift 🎁 !`
  String get gameWonPickABall {
    return Intl.message(
      'Pick a ball to reveal your gift 🎁 !',
      name: 'gameWonPickABall',
      desc: '',
      args: [],
    );
  }

  /// `Oops !`
  String get gameLostOops {
    return Intl.message(
      'Oops !',
      name: 'gameLostOops',
      desc: '',
      args: [],
    );
  }

  /// `The word to find was`
  String get gameLostWordNotFound {
    return Intl.message(
      'The word to find was',
      name: 'gameLostWordNotFound',
      desc: '',
      args: [],
    );
  }

  /// `Try again to win an awesome gift 🎁 !`
  String get gameLostTryAgain {
    return Intl.message(
      'Try again to win an awesome gift 🎁 !',
      name: 'gameLostTryAgain',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'fr'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
