import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:tusom/theme/theme.dart';

class Word extends Equatable {
  final List<GuessLetter> letters;
  final bool isSubmitted;

  bool get isEmpty => letters.every((l) => l.status.isGiven || l.isEmptyLetter);

  bool get isNotEmpty => !isEmpty;

  bool get isComplete => isSubmitted || !letters.any((l) => l.isEmptyLetter);

  @override
  List<Object?> get props => [letters, isSubmitted];

  const Word(this.letters, {this.isSubmitted = false});

  Word copyWithNewLetter(String char) {
    assert(!isComplete, 'Cannot add a char to a complete word');
    final indexToReplace = letters.indexWhere((l) => l.isEmptyLetter);
    final newLetters = [...letters];
    newLetters[indexToReplace] =
        GuessLetter(char, GuessLetterStatus.inProgress);
    return Word(newLetters);
  }

  Word copyWithDeletedLastLetter() {
    assert(isNotEmpty, 'Cannot remove letter from empty word');
    final indexToReplace =
        letters.lastIndexWhere((l) => !l.status.isGiven && !l.isEmptyLetter);
    final newLetters = [...letters];
    newLetters[indexToReplace] =
        const GuessLetter('.', GuessLetterStatus.inProgress);
    return Word(newLetters);
  }

  Word copy() => Word([...letters], isSubmitted: isSubmitted);

  @override
  String toString() {
    return """\nWord('${letters.map((e) => e.char).join('_')}')
      ${letters.map((_) => '↑').join(' ')}
      ${letters.map((l) => l.status.name[0]).join(' ')}    
""";
  }

  String toWordString() {
    return letters.map((e) => e.char).join();
  }
}

abstract class _Letter extends Equatable {
  final String char;

  const _Letter(this.char);
}

class GuessLetter extends _Letter {
  final GuessLetterStatus status;

  bool get isEmptyLetter => char == '' || char == '.';

  @override
  List<Object?> get props => [char, status];

  const GuessLetter(String char, this.status) : super(char);

  GuessLetter copyWithStatus(GuessLetterStatus newStatus) {
    return GuessLetter(char, newStatus);
  }

  KeyboardLetter toKeyboardLetter() {
    return KeyboardLetter(char, status.toKeyboardLetterStatus());
  }
}

class KeyboardLetter extends _Letter {
  final KeyboardLetterStatus status;

  @override
  List<Object?> get props => [char, status];

  const KeyboardLetter(String char, this.status) : super(char);
}

enum GuessLetterStatus {
  given,
  inProgress,
  notPresent,
  misplaced,
  correctlyPlaced,
}

extension GuessLetterStatusUtils on GuessLetterStatus {
  Color get backgroundColor {
    switch (this) {
      case GuessLetterStatus.given:
      case GuessLetterStatus.correctlyPlaced:
        return gameRed;
      default:
        return Colors.transparent;
    }
  }

  bool get isGiven => this == GuessLetterStatus.given;

  bool get isGivenOrCorrect =>
      isGiven || this == GuessLetterStatus.correctlyPlaced;

  KeyboardLetterStatus toKeyboardLetterStatus() {
    switch (this) {
      case GuessLetterStatus.given:
      case GuessLetterStatus.correctlyPlaced:
        return KeyboardLetterStatus.correctlyPlaced;
      case GuessLetterStatus.notPresent:
        return KeyboardLetterStatus.notPresent;
      case GuessLetterStatus.misplaced:
        return KeyboardLetterStatus.misplaced;
      default:
        return KeyboardLetterStatus.uncovered;
    }
  }

  bool isBetterThan(KeyboardLetterStatus kls) {
    assert(
      [
            GuessLetterStatus.correctlyPlaced,
            GuessLetterStatus.given,
            GuessLetterStatus.misplaced,
            GuessLetterStatus.notPresent,
          ].contains(this) &&
          [
            KeyboardLetterStatus.correctlyPlaced,
            KeyboardLetterStatus.misplaced,
            KeyboardLetterStatus.notPresent,
          ].contains(kls),
      'keyboard letter status can only be compared for valid statuses',
    );
    switch (kls) {
      case KeyboardLetterStatus.correctlyPlaced:
        return false;
      case KeyboardLetterStatus.misplaced:
        return isGivenOrCorrect;
      default:
        return true;
    }
  }

  String sound() {
    switch (this) {
      case GuessLetterStatus.given:
      case GuessLetterStatus.correctlyPlaced:
        return 'lettre-bien-place.wav';
      case GuessLetterStatus.misplaced:
        return 'lettre-mal-place.wav';
      default:
        return 'lettre-non-trouve.wav';
    }
  }
}

enum KeyboardLetterStatus {
  uncovered,
  notPresent,
  misplaced,
  correctlyPlaced,
}

extension KeyboardLetterStatusUtils on KeyboardLetterStatus {
  Color get backgroundColor {
    switch (this) {
      case KeyboardLetterStatus.correctlyPlaced:
        return gameRed;
      case KeyboardLetterStatus.misplaced:
        return gameYellow;
      case KeyboardLetterStatus.notPresent:
        return Colors.grey;
      case KeyboardLetterStatus.uncovered:
        return Colors.white;
    }
  }

  Color get textColor {
    switch (this) {
      case KeyboardLetterStatus.notPresent:
      case KeyboardLetterStatus.uncovered:
        return darkGrey;
      default:
        return Colors.white;
    }
  }
}
