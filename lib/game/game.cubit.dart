import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tusom/game/game.model.dart';
import 'package:tusom/game/game.state.dart';
import 'package:tusom/settings/settings.model.dart';

class GameCubit extends Cubit<GameState> {
  final Dictionary dictionary;
  late List<String> validationWords;

  GameCubit(String word, this.dictionary) : super(GameGuessing.initial(word)) {
    _initValidationWords(word);
  }

  void addLetter(String char) {
    final stateBefore = state;
    if (state is GameGuessing) {
      if (!state.currentTry!.isComplete) {
        emit((state as GameGuessing).copyWithNewLetter(char));
      } else {
        emit(GameWordAlreadyComplete(state.word, state.tries, state.keyboard));
        emit(stateBefore);
      }
    } else {
      debugPrint('Invalid state to add a letter : ${state.runtimeType}');
    }
  }

  void removeLastLetter() {
    final stateBefore = state;
    if (state is GameGuessing) {
      if (state.currentTry!.isNotEmpty) {
        emit((state as GameGuessing).copyWithDeletedLastLetter());
      } else {
        emit(GameWordAlreadyComplete(state.word, state.tries, state.keyboard));
        emit(stateBefore);
      }
    } else {
      debugPrint('Invalid state to remove a letter : ${state.runtimeType}');
    }
  }

  void resetGame(String newWord) {
    _initValidationWords(newWord);
    emit(GameGuessing.initial(newWord));
  }

  Future<void> submitWord() async {
    Future<void> _animateWordResult(List<Word> animatedWords) async {
      var letterIndex = 1;
      for (final word in animatedWords) {
        final tries = [...state.tries];
        tries[state.currentTryIndex!] = word;
        emit(
          GameAnimating(
            state.word,
            tries,
            state.keyboard,
            word.letters[letterIndex],
          ),
        );

        await Future.delayed(const Duration(milliseconds: 400));
        letterIndex++;
      }
    }

    void _setupNextTry() {
      var newState = (state as GameAnimating)
          .toGameGuessing()
          .copyWithNewComputeKeyboardState();

      final correctlyPlacedLetters = <int, GuessLetter>{};
      for (final aTry in newState.tries) {
        for (var i = 0; i < aTry.letters.length; i++) {
          if (aTry.letters[i].status.isGivenOrCorrect) {
            correctlyPlacedLetters[i] = aTry.letters[i];
          }
        }
      }
      final initialLettersForNewTry = <GuessLetter>[];
      for (var i = 0; i < newState.word.length; i++) {
        initialLettersForNewTry.add(
          correctlyPlacedLetters.containsKey(i)
              ? correctlyPlacedLetters[i]!
              : const GuessLetter('.', GuessLetterStatus.inProgress),
        );
      }
      newState = newState.replaceCurrentTry(Word(initialLettersForNewTry));
      emit(GameGuessing(newState.word, newState.tries, newState.keyboard));
    }

    final stateBefore = state;
    if (state is GameGuessing) {
      if (state.currentTry!.isComplete && !state.currentTry!.isSubmitted) {
        if (_isWordValid(state.currentTry!)) {
          final initialWord = state.currentTry!.copy();
          final processedWord = _processWord();
          final animatedWords = _animatedWords(initialWord, processedWord);

          await _animateWordResult(animatedWords);

          if (processedWord.toWordString() == state.word) {
            // 🏆 Victory !
            emit(GameWon(state.word, state.tries, state.keyboard));
          } else if (state.currentTry == null) {
            // ⚰️ Game's lost !
            emit(GameLost(state.word, state.tries, state.keyboard));
          } else {
            // 🎮 Game goes on to the next try !
            _setupNextTry();
          }
        } else {
          emit(GameWordNotValid(state.word, state.tries, state.keyboard));
          emit(stateBefore);
        }
      } else {
        emit(GameWordNotComplete(state.word, state.tries, state.keyboard));
        emit(stateBefore);
      }
    } else {
      debugPrint('Invalid state to submit a word : ${state.runtimeType}');
    }
  }

  void _initValidationWords(String word) {
    dictionary.validationWords(word).then((_validationWords) {
      validationWords = _validationWords;
    });
  }

  Word _processWord() {
    final initialWord = state.word;
    final word = state.currentTry!.copy();
    final correctlyPlacedLetters = <String, int>{};

    // First round: find correctly placed letters
    for (var i = 1; i < word.letters.length; i++) {
      if (initialWord[i] == word.letters[i].char) {
        word.letters[i] =
            word.letters[i].copyWithStatus(GuessLetterStatus.correctlyPlaced);
        correctlyPlacedLetters.update(
          initialWord[i],
          (count) => count + 1,
          ifAbsent: () => 1,
        );
      }
    }

    // Second round: find misplaced letters
    final misplacedLetters = <String, int>{};
    for (var i = 1; i < word.letters.length; i++) {
      final currentGuessLetter = word.letters[i].char;
      if (word.letters[i].status == GuessLetterStatus.inProgress) {
        final nbOccurencesInInitialWord = initialWord.codeUnits.fold<int>(
          0,
          (p, l) => p + (String.fromCharCode(l) == currentGuessLetter ? 1 : 0),
        );
        final nbCorrectlyPlaced =
            correctlyPlacedLetters.containsKey(currentGuessLetter)
                ? correctlyPlacedLetters[currentGuessLetter]!
                : 0;
        final nbMisplaced = misplacedLetters.containsKey(currentGuessLetter)
            ? misplacedLetters[currentGuessLetter]!
            : 0;
        final firstLetter =
            word.letters.first.char == currentGuessLetter ? 1 : 0;

        if (nbOccurencesInInitialWord >
            (nbCorrectlyPlaced + nbMisplaced + firstLetter)) {
          word.letters[i] =
              word.letters[i].copyWithStatus(GuessLetterStatus.misplaced);
          misplacedLetters.update(
            currentGuessLetter,
            (count) => count + 1,
            ifAbsent: () => 1,
          );
        } else {
          word.letters[i] =
              word.letters[i].copyWithStatus(GuessLetterStatus.notPresent);
        }
      }
    }
    return Word(word.letters, isSubmitted: true);
  }

  List<Word> _animatedWords(Word initialTry, Word processedTry) {
    return List.generate(initialTry.letters.length - 1, (i) => i + 1).map((i) {
      if (i == initialTry.letters.length - 1) {
        return processedTry;
      }
      return Word([
        ...processedTry.letters.sublist(0, i + 1),
        ...initialTry.letters.sublist(i + 1),
      ]);
    }).toList();
  }

  bool _isWordValid(Word word) {
    return validationWords.contains(word.toWordString().toLowerCase());
  }
}
