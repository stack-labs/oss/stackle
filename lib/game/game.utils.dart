import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:tusom/settings/settings.model.dart';

const maxTries = 6;

const keyboardRows = [
  ['A', 'Z', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'],
  ['Q', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M'],
  ['W', 'X', 'C', 'V', 'B', 'N'],
];

List<String> alphabet() {
  final startCode = 'A'.codeUnitAt(0);
  return List.generate(26, (i) => startCode + i)
      .map(String.fromCharCode)
      .toList();
}

String randomWord(Settings settings) {
  final subDictionary = settings.dictionary.words
      .where(
        (w) =>
            w.length >= settings.wordMinLength &&
            w.length <= settings.wordMaxLength,
      )
      .toList();
  if (subDictionary.isEmpty) {
    final message =
        'Dictionary ${settings.dictionary.name} contains no words that satisfies min length of ${settings.wordMinLength} and max length of ${settings.wordMaxLength}';
    debugPrint(message);
    throw message;
  }
  final randomIndex = Random().nextInt(subDictionary.length);
  return subDictionary[randomIndex].toUpperCase();
}
