import 'package:flutter/material.dart';
import 'package:tusom/game/game.model.dart';
import 'package:tusom/theme/theme.dart';

class WordLine extends StatelessWidget {
  final Word word;
  final double letterSize;

  const WordLine({
    required this.word,
    required this.letterSize,
  }) : super();

  @override
  Widget build(BuildContext context) {
    Widget _letter(GuessLetter letter) {
      return Text(
        letter.char,
        style: const TextStyle(
          fontSize: 22,
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      );
    }

    return Row(
      children: word.letters
          .map(
            (letter) => Expanded(
              child: Container(
                height: letterSize,
                decoration: BoxDecoration(
                  color: letter.status.backgroundColor,
                  border: whiteBorder,
                ),
                child: Center(
                  child: letter.status == GuessLetterStatus.misplaced
                      ? ClipOval(
                          child: Container(
                            width: double.infinity,
                            height: double.infinity,
                            color: gameYellow,
                            child: Center(child: _letter(letter)),
                          ),
                        )
                      : _letter(letter),
                ),
              ),
            ),
          )
          .toList(),
    );
  }
}
