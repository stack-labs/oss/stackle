import 'package:equatable/equatable.dart';
import 'package:tusom/game/game.model.dart';
import 'package:tusom/game/game.utils.dart';

abstract class GameState extends Equatable {
  final String word;
  final List<Word> tries;
  final Map<String, KeyboardLetter> keyboard;

  Word? get currentTry {
    final index = currentTryIndex;
    if (index != null) {
      return tries[index];
    }
    return null;
  }

  int? get currentTryIndex {
    try {
      return tries
          .asMap()
          .entries
          .firstWhere(
            (entry) => !entry.value.isSubmitted,
          )
          .key;
    } catch (_) {
      return null;
    }
  }

  @override
  List<Object?> get props => [word, tries, keyboard];

  const GameState(this.word, this.tries, this.keyboard);

  @override
  String toString() {
    return '$runtimeType({word: $word, tries: $tries})';
  }
}

class GameGuessing extends GameState {
  const GameGuessing(
    String word,
    List<Word> tries,
    Map<String, KeyboardLetter> keyboard,
  ) : super(word, tries, keyboard);

  factory GameGuessing.initial(String word) {
    final _uppercaseWord = word.toUpperCase();
    final keyboard = <String, KeyboardLetter>{
      _uppercaseWord[0]: KeyboardLetter(
        _uppercaseWord[0],
        KeyboardLetterStatus.correctlyPlaced,
      ),
    };
    final tries = [
      Word(
        [
          GuessLetter(_uppercaseWord[0], GuessLetterStatus.given),
          ...List.generate(
            _uppercaseWord.length - 1,
            (_) => const GuessLetter('.', GuessLetterStatus.inProgress),
          ),
        ],
      ),
      ...List.generate(
        maxTries - 1,
        (_) => Word(
          [
            ...List.generate(
              _uppercaseWord.length,
              (_) => const GuessLetter('', GuessLetterStatus.inProgress),
            ),
          ],
        ),
      ),
    ];
    return GameGuessing(_uppercaseWord, tries, keyboard);
  }

  GameGuessing copyWithNewLetter(String char) {
    final newState = GameGuessing(word, [...tries], keyboard);
    newState.tries[currentTryIndex!] = currentTry!.copyWithNewLetter(char);
    return newState;
  }

  GameGuessing copyWithDeletedLastLetter() {
    final newState = GameGuessing(word, [...tries], keyboard);
    newState.tries[currentTryIndex!] = currentTry!.copyWithDeletedLastLetter();
    return newState;
  }

  GameGuessing replaceCurrentTry(Word processedWord) {
    final newState = GameGuessing(word, [...tries], keyboard);
    newState.tries[currentTryIndex!] = processedWord;
    return newState;
  }

  GameGuessing copyWithNewComputeKeyboardState() {
    final guessedLetters = tries.fold<List<GuessLetter>>(
      <GuessLetter>[],
      (letters, word) => letters..addAll(word.letters),
    ).where((l) => !l.isEmptyLetter);
    final newKeyboard = <String, KeyboardLetter>{};
    for (final GuessLetter letter in guessedLetters) {
      if (!newKeyboard.containsKey(letter.char) ||
          letter.status.isBetterThan(newKeyboard[letter.char]!.status)) {
        newKeyboard[letter.char] = letter.toKeyboardLetter();
      }
    }
    return GameGuessing(word, tries, newKeyboard);
  }
}

abstract class GameOver extends GameState {
  const GameOver(
    String word,
    List<Word> tries,
    Map<String, KeyboardLetter> keyboard,
  ) : super(word, tries, keyboard);
}

class GameWon extends GameOver {
  int get nbTries =>
      tries
          .asMap()
          .entries
          .lastWhere(
            (entry) => entry.value.isComplete,
          )
          .key +
      1;

  const GameWon(
    String word,
    List<Word> tries,
    Map<String, KeyboardLetter> keyboard,
  ) : super(word, tries, keyboard);
}

class GameLost extends GameOver {
  const GameLost(
    String word,
    List<Word> tries,
    Map<String, KeyboardLetter> keyboard,
  ) : super(word, tries, keyboard);
}

class GameAnimating extends GameState {
  final GuessLetter currentLetter;

  @override
  List<Object?> get props => [
        ...super.props,
        currentLetter,
      ];

  const GameAnimating(
    String word,
    List<Word> tries,
    Map<String, KeyboardLetter> keyboard,
    this.currentLetter,
  ) : super(word, tries, keyboard);

  GameGuessing toGameGuessing() {
    return GameGuessing(word, tries, keyboard);
  }
}

// ------------- ERRORS STATES -------------
class GameWordAlreadyComplete extends GameState {
  const GameWordAlreadyComplete(
    String word,
    List<Word> tries,
    Map<String, KeyboardLetter> keyboard,
  ) : super(word, tries, keyboard);
}

class GameWordAlreadyEmpty extends GameState {
  const GameWordAlreadyEmpty(
    String word,
    List<Word> tries,
    Map<String, KeyboardLetter> keyboard,
  ) : super(word, tries, keyboard);
}

class GameWordNotComplete extends GameState {
  const GameWordNotComplete(
    String word,
    List<Word> tries,
    Map<String, KeyboardLetter> keyboard,
  ) : super(word, tries, keyboard);
}

class GameWordNotValid extends GameState {
  const GameWordNotValid(
    String word,
    List<Word> tries,
    Map<String, KeyboardLetter> keyboard,
  ) : super(word, tries, keyboard);
}
