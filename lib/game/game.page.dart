import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:tusom/game/game.cubit.dart';
import 'package:tusom/game/game.model.dart';
import 'package:tusom/game/game.state.dart';
import 'package:tusom/game/game.utils.dart';
import 'package:tusom/game/game_result.widget.dart';
import 'package:tusom/game/word_line.widget.dart';
import 'package:tusom/generated/l10n.dart';
import 'package:tusom/theme/theme.dart';
import 'package:url_launcher/url_launcher.dart';

const letterMaxSize = 75;

// TODO We could probably go stateless ?!
class GamePage extends StatefulWidget {
  const GamePage({Key? key}) : super(key: key);

  @override
  _GamePageState createState() => _GamePageState();
}

class _GamePageState extends State<GamePage> {
  final focusNode = FocusNode();
  late final letterFinalSize = min(
        MediaQuery.of(context).size.width,
        letterMaxSize * initialWord.length.toDouble(),
      ) /
      initialWord.length;

  final _myPlayer = FlutterSoundPlayer();
  var _mPlayerIsInited = false;

  GameCubit get game => context.read<GameCubit>();

  String get initialWord => game.state.word;

  @override
  void initState() {
    super.initState();
    _myPlayer.openPlayer().then((value) {
      setState(() {
        _mPlayerIsInited = true;
      });
    });
  }

  @override
  void dispose() {
    _myPlayer.closePlayer();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardListener(
      autofocus: true,
      focusNode: focusNode,
      onKeyEvent: (KeyEvent event) {
        if (event is KeyUpEvent) {
          if (event.logicalKey.keyId >= 97 && event.logicalKey.keyId <= 122) {
            game.addLetter(event.logicalKey.keyLabel);
          } else if (event.logicalKey.keyId == 4294967304 ||
              event.logicalKey.keyLabel == 'Backspace') {
            game.removeLastLetter();
          } else if (event.logicalKey.keyId == 4294967309 ||
              event.logicalKey.keyLabel == 'Enter' ||
              event.logicalKey.keyLabel == 'Numpad Enter') {
            game.submitWord();
          }
        }
      },
      child: Scaffold(
        appBar: context.stackAppBar(
          actions: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  onPressed: () {
                    launch('https://dev.to/stack-labs');
                  },
                  child: const Padding(
                    padding: EdgeInsets.only(right: 8.0),
                    child: Text(
                      'dev.to/stack-labs',
                      style: TextStyle(
                        fontSize: 25,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        body: BlocConsumer<GameCubit, GameState>(
          listener: (context, state) {
            if (state is GameAnimating && _mPlayerIsInited) {
              _myPlayer.startPlayer(
                fromURI:
                    '${kIsWeb ? "assets/" : ""}assets/sounds/${state.currentLetter.status.sound()}',
                codec: Codec.pcm16WAV,
              );
            } else if (state is GameWordNotValid) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  behavior: SnackBarBehavior.floating,
                  backgroundColor: Colors.red.shade50,
                  width: kIsWeb ? MediaQuery.of(context).size.width / 2 : null,
                  content: Text(
                    S.of(context).gameWordNotInDictionary,
                    style: TextStyle(
                      fontSize:
                          Theme.of(context).textTheme.bodyText2!.fontSize! + 2,
                      color: Colors.red,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              );
            }
          },
          builder: (context, state) {
            final maxWidth = min(
              MediaQuery.of(context).size.width,
              letterMaxSize * initialWord.length.toDouble(),
            );
            return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ConstrainedBox(
                  constraints: BoxConstraints(
                    maxWidth: maxWidth,
                  ),
                  child: Stack(
                    children: [
                      SizedBox.fromSize(size: MediaQuery.of(context).size),
                      ConstrainedBox(
                        constraints: BoxConstraints(
                          maxHeight: MediaQuery.of(context).size.height -
                              context.footerHeight,
                        ),
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              const SizedBox(height: 25),
                              Container(
                                decoration: BoxDecoration(border: whiteBorder),
                                child: Column(
                                  children: state.tries
                                      .map(
                                        (w) => WordLine(
                                          word: w,
                                          letterSize: letterFinalSize,
                                        ),
                                      )
                                      .toList(),
                                ),
                              ),
                              SizedBox(
                                height:
                                    state is GameOver ? 16 : letterFinalSize,
                              ),
                              if (state is! GameOver)
                                ..._keyboardRows()
                              else
                                GameResultWidget(gameOver: state),
                            ],
                          ),
                        ),
                      ),
                      Positioned(
                        left: (maxWidth - context.footerWidth) / 2,
                        bottom: 60,
                        child: SafeArea(child: context.stackFooter()),
                      ),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  List<Widget> _keyboardRows() {
    return keyboardRows
        .asMap()
        .entries
        .expand(
          (keyboardRow) => [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                ..._buildKeyboardLettersRow(keyboardRow),
                if (keyboardRow.key == 2) ...[
                  _buildBackspaceKey(),
                  const SizedBox(width: 5),
                  _buildEnterKey(),
                ],
              ],
            ),
            const SizedBox(height: 5),
          ],
        )
        .toList();
  }

  List<Widget> _buildKeyboardLettersRow(
    MapEntry<int, List<String>> keyboardRow,
  ) {
    final keyboard = game.state.keyboard;
    return keyboardRow.value.expand(
      (letter) {
        final KeyboardLetter kLetter = keyboard.containsKey(letter)
            ? keyboard[letter]!
            : KeyboardLetter(letter, KeyboardLetterStatus.uncovered);
        return [
          InkWell(
            onTap: () => game.addLetter(letter),
            child: Container(
              width: 30,
              height: 40,
              decoration: BoxDecoration(
                color: kLetter.status.backgroundColor,
                borderRadius: BorderRadius.circular(2),
              ),
              child: Center(
                child: Text(
                  letter.toUpperCase(),
                  style: TextStyle(color: kLetter.status.textColor),
                ),
              ),
            ),
          ),
          const SizedBox(width: 5),
        ];
      },
    ).toList();
  }

  Widget _buildBackspaceKey() {
    final currentTry = game.state.currentTry;
    return InkWell(
      onTap: () => game.removeLastLetter(),
      child: Container(
        width: 60,
        height: 40,
        decoration: BoxDecoration(
          color: currentTry != null &&
                  !currentTry.isSubmitted &&
                  currentTry.isNotEmpty
              ? KeyboardLetterStatus.uncovered.backgroundColor
              : KeyboardLetterStatus.notPresent.backgroundColor,
          borderRadius: BorderRadius.circular(2),
        ),
        child: const Center(
          child: Icon(
            Icons.backspace,
            size: 17,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  Widget _buildEnterKey() {
    final currentTry = game.state.currentTry;
    return InkWell(
      onTap: () {
        game.submitWord();
      },
      child: Container(
        width: 60,
        height: 40,
        decoration: BoxDecoration(
          color: currentTry != null &&
                  currentTry.isComplete &&
                  !currentTry.isSubmitted
              ? KeyboardLetterStatus.uncovered.backgroundColor
              : KeyboardLetterStatus.notPresent.backgroundColor,
          borderRadius: BorderRadius.circular(2),
        ),
        child: const Center(
          child: Icon(
            Icons.send,
            size: 17,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
