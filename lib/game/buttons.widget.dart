import 'package:flutter/material.dart';

class RoundButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;

  const RoundButton({
    required this.text,
    required this.onPressed,
  }) : super();

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        shape: const CircleBorder(),
        padding: const EdgeInsets.all(60),
      ),
      child: Text(
        text.split(' ').join('\n'),
        textAlign: TextAlign.center,
        style: const TextStyle(
          fontSize: 22,
        ),
      ),
    );
  }
}
