import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tusom/game/buttons.widget.dart';
import 'package:tusom/game/game.cubit.dart';
import 'package:tusom/game/game.state.dart';
import 'package:tusom/game/game.utils.dart';
import 'package:tusom/generated/l10n.dart';
import 'package:tusom/settings/settings_cubit.dart';

class GameResultWidget extends StatelessWidget {
  final GameOver gameOver;

  const GameResultWidget({
    required this.gameOver,
  }) : super();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (gameOver is GameWon)
          GameWonWidget(gameWon: gameOver as GameWon)
        else
          GameLostWidget(gameLost: gameOver as GameLost),
        const SizedBox(height: 32),
        RoundButton(
          onPressed: () {
            context.read<GameCubit>().resetGame(
                  randomWord(context.read<SettingsCubit>().state),
                );
          },
          text: S.of(context).newGame,
        ),
      ],
    );
  }
}

class GameWonWidget extends StatelessWidget {
  final GameWon gameWon;

  const GameWonWidget({
    required this.gameWon,
  }) : super();

  @override
  Widget build(BuildContext context) {
    final defaultStyle = Theme.of(context).textTheme.bodyText2!.copyWith(
          fontSize: Theme.of(context).textTheme.bodyText2!.fontSize! + 2,
        );
    final boldStyle = defaultStyle.copyWith(fontWeight: FontWeight.bold);
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
        style: defaultStyle,
        children: [
          TextSpan(
            text: '${S.of(context).gameWonCongratz.toUpperCase()} ',
            style: boldStyle,
          ),
          TextSpan(text: '${S.of(context).gameWonFoundTheWord} '),
          TextSpan(
            text: gameWon.word,
            style: boldStyle,
          ),
          TextSpan(text: ' ${S.of(context).gameWonNbTries(gameWon.nbTries)}'),
          TextSpan(text: '\n${S.of(context).gameWonPickABall}'),
        ],
      ),
    );
  }
}

class GameLostWidget extends StatelessWidget {
  final GameLost gameLost;

  const GameLostWidget({
    required this.gameLost,
  }) : super();

  @override
  Widget build(BuildContext context) {
    final defaultStyle = Theme.of(context).textTheme.bodyText2!.copyWith(
          fontSize: Theme.of(context).textTheme.bodyText2!.fontSize! + 2,
        );
    final boldStyle = defaultStyle.copyWith(fontWeight: FontWeight.bold);
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
        style: defaultStyle,
        children: [
          TextSpan(
            text: '${S.of(context).gameLostOops.toUpperCase()} ',
            style: boldStyle,
          ),
          TextSpan(text: ' ${S.of(context).gameLostWordNotFound} '),
          TextSpan(
            text: gameLost.word,
            style: boldStyle,
          ),
          const TextSpan(text: '.'),
          TextSpan(text: '\n${S.of(context).gameLostTryAgain}'),
        ],
      ),
    );
  }
}
