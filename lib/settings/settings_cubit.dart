import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tusom/settings/settings.model.dart';

class SettingsCubit extends HydratedCubit<Settings> {
  SettingsCubit() : super(const Settings());

  void updateDictionary(Dictionary dictionary) {
    emit(state.copyWith(dictionary: dictionary));
  }

  void updateWordsLengthConstraints(int minWordsLength, int maxWordsLength) {
    emit(
      state.copyWith(
        wordMinLength: minWordsLength,
        wordMaxLength: maxWordsLength,
      ),
    );
  }

  @override
  Settings fromJson(Map<String, dynamic> json) {
    return Settings(
      dictionary: Dictionary.values.byName(json['dictionary'] as String),
      wordMinLength: json['wordMinLength'] as int,
      wordMaxLength: json['wordMaxLength'] as int,
    );
  }

  @override
  Map<String, dynamic> toJson(Settings state) {
    return {
      'dictionary': state.dictionary.name,
      'wordMinLength': state.wordMinLength,
      'wordMaxLength': state.wordMaxLength,
    };
  }
}
