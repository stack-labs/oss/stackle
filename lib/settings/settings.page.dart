import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tusom/generated/l10n.dart';
import 'package:tusom/settings/settings.model.dart';
import 'package:tusom/settings/settings_cubit.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(S.of(context).settingsTitle)),
      body: FractionallySizedBox(
        widthFactor: 0.8,
        child: BlocBuilder<SettingsCubit, Settings>(
          builder: (_, settings) => Form(
            child: ListView(
              padding: const EdgeInsets.all(16),
              children: [
                Text(S.of(context).settingsDictionary),
                ...Dictionary.values.map(
                  (dico) => RadioListTile<Dictionary>(
                    contentPadding: EdgeInsets.zero,
                    title: Text(
                      S.of(context).dictionaryWithFlag(dico.name),
                      style: const TextStyle(color: Colors.white),
                    ),
                    selected: settings.dictionary == dico,
                    value: dico,
                    groupValue: settings.dictionary,
                    onChanged: (_) =>
                        context.read<SettingsCubit>().updateDictionary(dico),
                    activeColor: Colors.white,
                    selectedTileColor: Colors.white.withOpacity(0.1),
                  ),
                ),
                const SizedBox(height: 16),
                Text(S.of(context).settingsWordsLength),
                RangeSlider(
                  min: 5,
                  max: 12,
                  values: RangeValues(
                    settings.wordMinLength.toDouble(),
                    settings.wordMaxLength.toDouble(),
                  ),
                  divisions: 12 - 5,
                  labels: RangeLabels(
                    'Min: ${settings.wordMinLength.toDouble()}',
                    'Max: ${settings.wordMaxLength.toDouble()}',
                  ),
                  onChanged: (rangeValues) {
                    context.read<SettingsCubit>().updateWordsLengthConstraints(
                          rangeValues.start.round(),
                          rangeValues.end.round(),
                        );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
