import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:tusom/game/dictionaries/english_dictionary.dart';
import 'package:tusom/game/dictionaries/french_dictionary.dart';
import 'package:tusom/game/dictionaries/tech_dictionary.dart';

class Settings extends Equatable {
  final Dictionary dictionary;
  final int wordMinLength;
  final int wordMaxLength;

  @override
  List<Object?> get props => [dictionary, wordMinLength, wordMaxLength];

  const Settings({
    this.dictionary = Dictionary.french,
    this.wordMinLength = 5,
    this.wordMaxLength = 8,
  });

  Settings copyWith({
    Dictionary? dictionary,
    int? wordMinLength,
    int? wordMaxLength,
  }) {
    return Settings(
      dictionary: dictionary ?? this.dictionary,
      wordMinLength: wordMinLength ?? this.wordMinLength,
      wordMaxLength: wordMaxLength ?? this.wordMaxLength,
    );
  }
}

enum Dictionary { english, french, tech }

extension DictionaryUtils on Dictionary {
  Future<List<String>> validationWords(String forWord) async {
    final completeValidationDictionary =
        this == Dictionary.tech ? 'english' : name;
    final completeLanguageValidationWords = await rootBundle
        .loadString(
          'assets/words/$completeValidationDictionary/words-${forWord[0].toUpperCase()}-${forWord.length}.txt',
        )
        .then(LineSplitter.split)
        .catchError((Object e) {
      debugPrint('Error while reading words file : $e');
      throw e;
    });
    return [
      ...words,
      ...completeLanguageValidationWords.toList(),
    ];
  }

  List<String> get words {
    switch (this) {
      case Dictionary.english:
        return englishDictionary;
      case Dictionary.french:
        return frenchDictionary;
      case Dictionary.tech:
        return techDictionary;
    }
  }
}
