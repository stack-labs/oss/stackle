import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

const gameYellow = Color(0xFFFFCA47);
const gameRed = Color(0xFFFF4747);
const stackColor = Color(0xFF4559FF);
const lightGrey = Color(0xFFD0D0D0);
const darkGrey = Color(0xFF383838);

final stackTheme = ThemeData(
  colorScheme: const ColorScheme.light().copyWith(
    primary: stackColor,
  ),
  scaffoldBackgroundColor: stackColor,
  appBarTheme: const AppBarTheme(
    elevation: 0,
  ),
  textTheme: const TextTheme(
    bodyText2: TextStyle(
      fontSize: kIsWeb ? 18 : 16,
      color: Colors.white,
    ),
  ).apply(
    bodyColor: Colors.white,
    displayColor: Colors.white,
  ),
  radioTheme: RadioThemeData(
    fillColor: MaterialStateProperty.all(Colors.white),
    overlayColor: MaterialStateProperty.all(Colors.white),
  ),
  sliderTheme: SliderThemeData(
    activeTrackColor: Colors.white,
    inactiveTrackColor: lightGrey.withOpacity(0.8),
    thumbColor: gameYellow,
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      shape: MaterialStateProperty.all(
        const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(12),
          ),
        ),
      ),
      padding: MaterialStateProperty.all(
        const EdgeInsets.symmetric(vertical: 20, horizontal: 30),
      ),
      textStyle: MaterialStateProperty.all(
        const TextStyle(fontSize: 16),
      ),
    ),
  ),
  outlinedButtonTheme: OutlinedButtonThemeData(
    style: ButtonStyle(
      shape: MaterialStateProperty.all(
        const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(30),
          ),
        ),
      ),
      side: MaterialStateProperty.all(
        const BorderSide(
          color: Colors.white,
          width: 2,
        ),
      ),
      foregroundColor: MaterialStateProperty.all(Colors.white),
      padding: MaterialStateProperty.all(
        const EdgeInsets.symmetric(vertical: 20, horizontal: 30),
      ),
      textStyle: MaterialStateProperty.all(
        const TextStyle(fontSize: 18),
      ),
    ),
  ),
  textButtonTheme: TextButtonThemeData(
    style: ButtonStyle(
      foregroundColor: MaterialStateProperty.all(Colors.white),
    ),
  ),
);

extension AppBarUtils on BuildContext {
  AppBar stackAppBar({List<Widget>? actions}) {
    return AppBar(
      toolbarHeight: 80,
      title: SvgPicture.asset(
        'assets/images/stackle-logo.svg',
        width: 300,
      ),
      bottom: PreferredSize(
        preferredSize: Size(MediaQuery.of(this).size.width, 3),
        child: Column(
          children: [
            Container(
              height: 3,
              width: MediaQuery.of(this).size.width,
              color: Colors.white,
            ),
          ],
        ),
      ),
      actions: actions,
    );
  }

  Widget stackFooter() {
    return ConstrainedBox(
      constraints: BoxConstraints(
        maxWidth: footerWidth,
      ),
      child: Image.asset('assets/images/stack-labs-logo-white.png'),
    );
  }

  double get footerWidth => min(MediaQuery.of(this).size.width * 0.5, 300);

  double get footerHeight => footerWidth * 60 / 482;
}

final whiteBorder = Border.all(color: Colors.white);
