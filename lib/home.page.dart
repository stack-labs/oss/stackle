import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tusom/game/buttons.widget.dart';
import 'package:tusom/game/game.cubit.dart';
import 'package:tusom/game/game.page.dart';
import 'package:tusom/game/game.utils.dart';
import 'package:tusom/generated/l10n.dart';
import 'package:tusom/settings/settings.model.dart';
import 'package:tusom/settings/settings.page.dart';
import 'package:tusom/settings/settings_cubit.dart';
import 'package:tusom/theme/theme.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: context.stackAppBar(
        actions: [
          IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return const SettingsPage();
                  },
                ),
              );
            },
          ),
        ],
      ),
      body: SizedBox(
        width: double.infinity,
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(height: 20),
              Text(S.of(context).gameInstructions),
              const SizedBox(height: 40),
              Text(S.of(context).settingsDictionary),
              const SizedBox(height: 5),
              BlocBuilder<SettingsCubit, Settings>(
                builder: (context, state) {
                  return Text(
                    S.of(context).dictionaryWithFlag(state.dictionary.name),
                  );
                },
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(height: 40),
                    RoundButton(
                      onPressed: () => _startNewGame(context),
                      text: S.of(context).newGame,
                    ),
                  ],
                ),
              ),
              context.stackFooter(),
              const SizedBox(height: 60),
            ],
          ),
        ),
      ),
    );
  }

  void _startNewGame(BuildContext context) {
    final settings = context.read<SettingsCubit>().state;
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_) {
          return BlocProvider<GameCubit>(
            create: (_) => GameCubit(randomWord(settings), settings.dictionary),
            child: const GamePage(),
          );
        },
      ),
    );
  }
}
